﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Types;

namespace BussinesInterfaces
{
    public interface ILecturaExcel
    {
        Task<IEnumerable<RecordMaster>> ReadFile(string file);
    }
}
