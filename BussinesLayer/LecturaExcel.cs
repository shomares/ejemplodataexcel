﻿using BussinesInterfaces;
using ExcelParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Types;

namespace BussinesLayer
{
    public class LecturaExcel : ILecturaExcel
    {
        private readonly ContainerLayout container;

        public LecturaExcel(ContainerLayout container)
        {
            this.container = container;
        }
        public async Task<IEnumerable<RecordMaster>> ReadFile(string file)
        {
            var funciones = new FuncionesExcel();
            funciones.Container = this.container;
            var result = funciones.LoadExcel<RecordMaster>(file, "layout");
            return await Task.FromResult(result.ListaObjetos);
        }
    }
}
