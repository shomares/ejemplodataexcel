﻿using BussinesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Types;

namespace Ejemplo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILecturaExcel lectura;

        public HomeController(ILecturaExcel lectura)
        {
            this.lectura = lectura;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Read()
        {
            return Json(await this.lectura.ReadFile(@"C:\Users\TISMX\Documents\Tareas\Carga\Maestro.xlsx"), JsonRequestBehavior.AllowGet);
        }

    }
}
