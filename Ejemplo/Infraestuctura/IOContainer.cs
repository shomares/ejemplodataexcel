﻿using BussinesInterfaces;
using BussinesLayer;
using Ejemplo.Infraestuctura;
using ExcelParser;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace Ejemplo.Infraestuctura
{
    public static class NinjectWebCommon
    {

        private static readonly IKernel kernel;
        private static readonly ContainerLayout container;
        private static Bootstrapper boot;

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            boot = new Bootstrapper();
            boot.Initialize(CreateKernel);
        }


        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            boot.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(c => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }


        private static void RegisterServices(IKernel kernel)
        {
            var container = new ContainerLayout();
            container.RootPath = @"C:\Users\TISMX\Documents\Sources\Ejemplo\Ejemplo\Layouts\";
            container.Layouts.TryAdd("layout", new DescriptorLayout { Path = "LayoutMaster.xml" });
            kernel.Bind<ILecturaExcel>().To<LecturaExcel>();
            kernel.Bind<ContainerLayout>().ToConstant(container).InSingletonScope();
        }

    }
}