﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Types
{
    public class RecordMaster
    {
        public string NoParte { get; set; }
        public string PartDesc { get; set; }
        public int? InvIni { get; set; }

        public int? Snp { get; set; }

        public int? CapsShooter { get; set; }

        public int? LoteDeAbasto { get; set; }

        public string Ruta { get; set; }

        public int? OrdenCarga { get; set; }

        public string Porc_de_Consumo { get; set; }

        public string BOMKey { get; set; }
        public string DIF { get; set; }

        public string SCAN { get; set; }
        public string OrdendeDescarga { get; set; }

        public string UbicaciondeCarga { get; set; }

        public string UbicaciondeDescarga { get; set; }


    }
}
